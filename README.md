## Description
Mountain-car example using dual control formulation in paper [1]. 

The MATLAB code relies entrirely on [CasADi](https://web.casadi.org/) and [IPOPT](https://coin-or.github.io/Ipopt/) 

Run `dual_control_example.m` to execute the simulation, consider the number of dual steps and the number of samples per step as tuning knobs. 


## Authors and acknowledgment
[1] E. Arcari, L. Hewing, M. N. Zeilinger, “An Approximate Dynamic Programming Approach
for Dual Stochastic Model Predictive Control”, IFAC World Congress 2020

