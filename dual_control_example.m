clear all

% mountain car dynamics
% x(k+1) = [p(k+1)] = [p(k) + Ts*v(k)]
%          [v(k+1)] = [v(k) - Ts*cos(3*p(k))theta(1) + Ts*u(k)*theta(2)]

% p = x(1),v = x(2): position and velocity
% theta(1): actuator gain, theta(2): mass
% Ts: sampling time

Ts = 7; 
Phi = @(x,u) [Ts*u(1), -Ts*cos(3*x(1))];
f = @(x,u,theta) [x(1) + Ts*x(2); x(2) + Phi(x,u)*theta];

% dimensions
nx = 2; %state
nu = 1; %input
np = 2; %parameter

% parametric uncertainty:
mTheta = [0.002; 0.0025];
stdTheta = diag([0.001; 0.001]);

% noise afftecting the velocity dynamics
mW = 0; % non-zero case can be adressed by splitting w = w1 + w2 where w1 is constant component set at mean
stdW = 0.1e-2;

% length of the control task
Nbar = 60; 

plots = true;

%% controller generation

% prediction horizon
N = 15; 

% number of samples per each lookahead step (in this case number of lookahead steps L = 3)
Ns = [5,5,5]; 

% cost function
costfun = @(x,u) -x(1); 

% generate the controller
[sol_DMPC, fXdmpc, fUdmpc, fThetadmpc, fpTheta, fcost] = dual_controller(nx,np,nu,N,f,Phi,costfun,Ns,stdW); 

%% simulation
x_dual(1,:) = [-0.5, 0];


mThetaDual(1,:) = mTheta;
precThetaDual(1,:,:) = inv(stdTheta.^(2));

% realization of theta true to use for the simulations Nsim
theta_true = mTheta + stdTheta*randn(np,1);

%closed loop noise realization
Ws = mW + stdW*randn(Nbar,1); 

U = zeros(Nbar,N,prod(Ns));
X = zeros(Nbar,N,2,prod(Ns));

try
    for i=1:Nbar
        %% Dual Control
        if x_dual(i,1) <= 0.6
            aux = squeeze(precThetaDual(i,:,:));
            nNo = sum(cumprod(Ns));
            Wunwrap = mW + stdW*randn(nNo,1);
            Runwrap = randn(sum(cumprod(Ns)),2);
            param = [x_dual(i,:)';mThetaDual(i,:)'; aux(:); Wunwrap(:); Runwrap(:)];

            nNu = 1 + sum(cumprod(Ns)) + prod(Ns)*(N-length(Ns)-1);
            r_dual = sol_DMPC('x0',randn(nNu,1),'lbx',-ones(nNu,1),'ubx',ones(nNu,1),'p',param);

            Uunwrap = full(r_dual.x); 

            for k = 1:length(Ns)+1
                for j=1:prod(Ns(1:k-1))
                    U_dual{k}{j} = full(fUdmpc{k}{j}(Uunwrap));
                    X_dual{k}{j} = full(fXdmpc{k}{j}(Uunwrap,param));
                    ThetaS_dual{k}{j} = full(fThetadmpc{k}{j}(Uunwrap,param));
                    pTheta{k}{j} = full(fpTheta{k}{j}(Uunwrap,param));
                end

            end
            cost = full(fcost(Uunwrap,param));

            for k = 1: prod(Ns)
                idx = ceil(cumprod([1,Ns])/(prod(Ns)/k));
                for j=1:length(idx)-1
                    X(i,j,:,k) = X_dual{j}{idx(j)};
                    U(i,j,k) = U_dual{j}{idx(j)};
                end
                X(i,j+1:N,:,k) = X_dual{j+1}{k};
                U(i,j+1:N,k) = U_dual{j+1}{k};
            end
            x_dual(i+1,:) = f(x_dual(i,:),U(i,1,1),theta_true) + [0;1]*Ws(i);
        else
            x_dual(i+1,:) = x_dual(i,:);
        end


        if plots
            subplot(2,1,2)
            plot(squeeze(X(i,:,1,:)))
            title('Dual')
        end

        precThetaDual(i+1,:,:) = squeeze(precThetaDual(i,:,:)) + ...
            Phi(x_dual(i,:),U(i,1,1))'*Phi(x_dual(i,:),U(i,1,1))/(stdW.^2);
        target = x_dual(i+1,2) - x_dual(i,2);
        mThetaDual(i+1,:) = squeeze(precThetaDual(i+1,:,:))\(squeeze(precThetaDual(i,:,:))*mThetaDual(i,:)' + ...
            Phi(x_dual(i,:),U(i,1,1))'*target/(stdW.^2));

        %% plots
        if plots
            subplot(2,1,1)
            aux = -1.2:0.1:0.6;
            hold off
            plot(aux,sin(3*aux))
            hold on
            %plot(aux,costfun(aux))
            plot(x_dual(i+1,1),sin(3*x_dual(i+1,1)),'yx')
        end
    end
catch
   res.log = ['something wrong: Sample ' k 'iteration' i]
end

res.Ws = Ws;
res.X_Dual = X;
res.U_Dual = U;
res.mThetaDual = mThetaDual;
res.precThetaDual = precThetaDual;
res.ThetaS = ThetaS_dual;
res.mTheta = mTheta;
res.stdTheta = stdTheta;
res.ThetaSim = theta_true;
res.mW = mW;
res.stdW = stdW;
res.Ts = Ts;
save('result.mat','res')
