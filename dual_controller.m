function [sol_DMPC,fXdual, fUdual, fThetaS, fpThetaS, fcost] = dual_controller(nx,np,nu,N,f,Phi,costfun,Ns,stdW)
import casadi.*


xinit = SX.sym('x0',1,nx);

% length of lookahead
L = length(Ns); 

% sample allocation for exploration phase
for i = 1:L
    for j = 1:prod(Ns(1:i-1))
        wS{i}{j} = SX.sym(['wS' num2str(i) num2str(j)],1,Ns(i));
        rnoTheta{i}{j} = SX.sym(['rnoTheta' num2str(i) num2str(j)],np,Ns(i)); %standard normal samples
        mTheta{i}{j} = SX.sym(['mTheta' num2str(i) num2str(j)],np,1);
        precTheta{i}{j} = SX.sym(['precTheta' num2str(i) num2str(j)],np,np); %precision matrix (inverse of variance)
        Xdual{i}{j} = SX.sym(['Xdual' num2str(i) num2str(j)],1,nx);
        Udual{i}{j} = SX.sym(['Udual' num2str(i) num2str(j)],1,nu);
    end
end

% last step
for j = 1:prod(Ns(1:i))
    Xdual{i+1}{j} = SX.sym(['Xdual' num2str(i+1) num2str(j)],N-L,nx);
    Udual{i+1}{j} = SX.sym(['Udual' num2str(i+1) num2str(j)],N-L,nu);
    mTheta{i+1}{j} = SX.sym(['mTheta' num2str(i+1) num2str(j)],np,1);
    precTheta{i+1}{j} = SX.sym(['precTheta' num2str(i+1) num2str(j)],np,np);
    
end

Xdual{1}{1} = xinit;

% Dual (exploration/lookahead) phase
costs = 0;
for i=1:L
    %Sampling Theta & noise
    for j=1:prod(Ns(1:i-1))
        % sample theta
        %thetaS{i}{j} = mTheta{i}{j} + chol(inv(precTheta{i}{j}))'*rnoTheta{i}{j}; % full variance (involves computing chol + inverse)
        
        thetaS{i}{j} = mTheta{i}{j} + sqrt(1./diag(precTheta{i}{j})).*rnoTheta{i}{j}; % sample eclusing off-diagonal terms
        
        % compute cost in node
        costs = costs + 1/prod(Ns(1:i-1))*costfun(Xdual{i}{j},Udual{i}{j});
        
        for k=(j-1)*Ns(i)+1:j*Ns(i)
            % propagate system
            Xdual{i+1}{k}(1,:) = f(Xdual{i}{j},Udual{i}{j},thetaS{i}{j}(:,rem(k-1,Ns(i))+1)) ...
                + [0; 1]*wS{i}{j}(rem(k-1,Ns(i))+1);
            
            % parameter update
            precTheta{i+1}{k} = precTheta{i}{j} + ...
                Phi(Xdual{i}{j},Udual{i}{j})'*Phi(Xdual{i}{j},Udual{i}{j})/(stdW.^2);
            target = Xdual{i+1}{k}(1,2) - Xdual{i}{j}(1,2);
            mTheta{i+1}{k} = precTheta{i+1}{k}\(precTheta{i}{j}*mTheta{i}{j} + ...
                Phi(Xdual{i}{j},Udual{i}{j})'*target/(stdW.^2));
        end
    end
end

% exploitation phase
costpart = SX.sym('c',prod(Ns),1);
for j=1:prod(Ns)
    thetaS{L+1}{j} = mTheta{L+1}{j}; %maximum likelihood estimate, mW = 0;
    costpart(j) = 0;
    for i=1:N-L-1
        Xdual{L+1}{j}(i+1,:) = f(Xdual{L+1}{j}(i,:),Udual{L+1}{j}(i),thetaS{L+1}{j})';
       
        costpart(j) = costpart(j) + costfun(Xdual{L+1}{j}(i+1,:),Udual{L+1}{j}(i+1,:));
    end
    costs = costs + 1/prod(Ns)*costpart(j);
end

Wunwrap = []; Runwrap = []; Uunwrap = Udual{1}{:};
for j=1:L
    Wunwrap = [Wunwrap, [wS{j}{:}]];
    Runwrap = [Runwrap, [rnoTheta{j}{:}]];
    Uunwrap = [Uunwrap, reshape([Udual{j+1}{:}],1,prod(size([Udual{j+1}{:}])))];
end


param = [xinit(:);mTheta{1}{1}(:); precTheta{1}{1}(:); Wunwrap(:); Runwrap(:)];
fcost = Function('f',{Uunwrap,param},{costs});

for i = 1:L+1
    for j=1:prod(Ns(1:i-1))
        fXdual{i}{j} = Function('f',{Uunwrap,param},{Xdual{i}{j}});
        fUdual{i}{j} = Function('f',{Uunwrap},{Udual{i}{j}});
        fThetaS{i}{j} = Function('f',{Uunwrap,param},{thetaS{i}{j}});
        fpThetaS{i}{j} = Function('f',{Uunwrap,param},{precTheta{i}{j}});
    end
end

nlps = struct('x',Uunwrap, 'f',costs,'p',param);
sol_DMPC = nlpsol('S', 'ipopt', nlps);

